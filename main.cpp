#include <iostream>
#include <string>
#include <unordered_map>
#include <map>
#include<fstream> 
#include<deque>

using namespace std;
 
class BobbleDictionary
{
 
struct Trie
	{
		bool isWordCompleted;
		unordered_map<string, int > predictedWord; 
		multimap<int, string, greater <int>> predictedWord_Oredered;

		unordered_map<char, Trie*> nodeMap;
	};
 
	void destroyRecursive(Trie* node)
	{
		if (node != nullptr)
		{
			if (node->nodeMap.size() == 0)
			{
				delete(node);
				return;
			}

			for (auto it = node->nodeMap.begin(); it != node->nodeMap.end(); it++)
			{
				destroyRecursive((*it).second);
			}
		}
	}

public:

	BobbleDictionary()
	{
		root = getNewTrie();
	}

	~BobbleDictionary()
	{
		if (root != nullptr)
		{
			destroyRecursive(root);
		}
	}

private:

	Trie* root; 
	deque<pair<string, string>> biGram;
	deque<string> uniGram;
	

	Trie* getNewTrie()
	{
		Trie *node = new Trie;
		node->isWordCompleted = false;
		
		return node;
	}
	 
	int updateWordCount(multimap<int, string, greater <int>> &wordMap, int count, string word)
	{
		typedef multimap<int, string>::iterator iterator;
		std::pair<iterator, iterator> iterpair = wordMap.equal_range(count - 1);

		iterator it = iterpair.first;
		for (; it != iterpair.second; ++it) {
			if (it->second == word) {
				wordMap.erase(it);
				break;
			}
		}

		wordMap.insert(pair<int, string>(count, word));
		return 0;
	}

	int createBiGram(string filename)
	{
		fstream file;
		string word, prev_word;

		file.open(filename.c_str());
		if (!file)
		{
			cout << "Error!! can not open file!!!" << endl;
			return -1;
		}

		file >> word;
		prev_word = word;
		while (file >> word)
		{
			//cout << prev_word <<":"<< word << endl; // debug , see the bigram from file
			biGram.push_back(pair<string, string>(prev_word, word));
			prev_word = word;
		}

		return 0;
	}

	int createUniGram(string filename)
	{
		fstream file;
		string word;

		file.open(filename.c_str());
		if (!file)
		{
			cout << "Error!! can not open file!!!" << endl;
			return -1;
		}

		while (file >> word)
		{
			//cout << word << endl; // debug , see the UniGram from file
			uniGram.push_back(word);
		}
		return 0;
	}

public:

	int add_word(string prev_word, string word)
	{

		Trie *temp = root;

		for (int i = 0; i < prev_word.length(); i++)
		{
			char alphabate = prev_word[i];

			if (temp->nodeMap.find(alphabate) == temp->nodeMap.end())
			{
				temp->nodeMap[alphabate] = getNewTrie();
			}

			temp = temp->nodeMap[alphabate];
		}

		temp->isWordCompleted = true;
		if (temp->predictedWord.find(word) == temp->predictedWord.end())
		{
			temp->predictedWord[word] = 1;
			temp->predictedWord_Oredered.insert(pair<int, string>(1, word));
		}
		else
		{
			temp->predictedWord[word]++;
			int count = temp->predictedWord[word];

			updateWordCount(temp->predictedWord_Oredered, count, word);			 
		}

		return 0;
	}

	int get_predictions(string word)
	{
		Trie *temp = root;

		cout << "Predictions for the Word: "<< word << endl;

		for (int i = 0; i < word.length(); i++)
		{
			char alphabate = word[i];
			temp = temp->nodeMap[alphabate];

			if (temp == nullptr)
			{
				cout << "Predictions not found" << "\n\n" << endl;
				return 0;
			}
		}

		if (!temp->isWordCompleted)
		{
			cout << "Predictions not found" << "\n\n" << endl;
			return 0;
		}

		auto it = temp->predictedWord_Oredered.begin();
		cout << "Predicted word => " << (*it).second << " : count = " << (*it).first << endl;

		it++;
		if((it)  == temp->predictedWord_Oredered.end())
		{
			cout << "No more predictions found" << "\n\n" << endl;
			return 0;
		}

		cout << "Predicted word => " << (*it).second << " : count = " << (*it).first << endl;

		it++;
		if ((it) == temp->predictedWord_Oredered.end())
		{
			cout << "No more predictions found" << "\n\n" << endl;
			return 0;
		}

		cout << "Predicted word => " << (*it).second << " : count = " << (*it).first <<"\n\n"<< endl;

		return 0;
	}

	int createDictionary( string filename)
	{

		if (createBiGram(filename) == -1)
		{
			cout << "Error ! Issue in biGram creation" << endl;
			return -1;
		}

		for (auto it = biGram.begin(); it != biGram.end(); it++)
		{
			add_word((*it).first, (*it).second);
		}

		return 0;
	}

	int prediction_input(string filename)
	{
		if (createUniGram(filename) == -1)
		{
			cout << "Error ! Issue in UniGram creation" << endl;
			return -1;
		}
		 
		for (auto it = uniGram.begin(); it != uniGram.end(); it++)
		{
			get_predictions((*it).data());
		}
		
		return 0;
	}
};

int main(int argc, char *argv[])
{
	BobbleDictionary d;

	if (argv[1] == nullptr)
	{
		cout << "please provide text_corpus file path and run again" << endl;
		return 0;
	}
	d.createDictionary(argv[1]);

	if (argv[2] == nullptr)
	{
		cout << "please provide input_words file path and run again" << endl;
		return 0;
	}
	d.prediction_input(argv[2]);
	 
	return 0;
}